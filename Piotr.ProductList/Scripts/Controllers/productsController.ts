/// <reference path='angular.d.ts' />
/// <reference path='model.ts' />

module Products {

    export interface Scope {
        newProductName: string;
        newProductPrice: number;
        products: Model.Product[];
        addNewProduct: Function;
        deleteProduct: Function;
    }

    export class Controller {
        private httpService: any;

        constructor ($scope: Scope, $http: any) {
            this.httpService = $http;

            this.refreshProducts($scope);

            var controller = this;

            $scope.addNewProduct = function () {
                var newProduct = new Model.Product();
                newProduct.Name = $scope.newProductName;
                newProduct.Price = $scope.newProductPrice;

                controller.addProduct(newProduct, function () {
                    controller.getAllProducts(function (data) {
                        $scope.products = data;
                    });
                });
            };

            $scope.deleteProduct = function (productId) {
                controller.deleteProduct(productId, function () {
                    controller.getAllProducts(function (data) {
                        $scope.products = data;
                    });
                });
            }
        }

        getAllProducts(successCallback: Function): void{
            this.httpService.get('/api/products').success(function (data, status) {
                successCallback(data);
            });
        }

        addProduct(product: Model.Product, successCallback: Function): void {
            this.httpService.post('/api/products', product).success(function () {
                successCallback();
            });
        }

        deleteProduct(productId: string, successCallback: Function): void {
            this.httpService.delete('/api/products/'+productId).success(function () {
                successCallback();
            });
        }
        
        refreshProducts(scope: Scope) {
            this.getAllProducts(function (data) {
                        scope.products = data;
                    });
        }

    }
}







